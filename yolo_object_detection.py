import cv2
import numpy as np 
import glob 
import random
import matplotlib.pyplot as plt

# Load Yolo
net = cv2.dnn.readNet("yolov3_training_last.weights", "yolov3_testing.cfg")

# Name custom object
classes = ["egg"]

# Images path
gray_img = glob.glob(r"C:\Users\Tamakot\Desktop\a\*.jpg")

layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))

# Insert here the path of your images
random.shuffle(gray_img)
# loop through all the images
for img_path in gray_img:
    # Loading image
    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=0.4, fy=0.4)
    height, width, channels = img.shape

    # Detecting objects
    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)

    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.3:
                # Object detected
                print(class_id)
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    print(indexes)
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            color = colors[class_ids[i]]
            cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
            cv2.putText(img, label, (x, y + 30), font, 3, color, 2)

    cv2.imshow("Image", img)

    gray_img = cv2.medianBlur(img,3)
    hsv = cv2.cvtColor(gray_img,cv2.COLOR_BGR2HSV)
    hsv1 = cv2.cvtColor(gray_img,cv2.COLOR_BGR2HSV)

    Canny = cv2.Canny(gray_img,200,100) #155,300
    thresh , result = cv2.threshold(Canny,170,255,cv2.THRESH_BINARY_INV)
    kernel = np.ones((2,2),np.uint8)

    opening = cv2.morphologyEx(result,cv2.MORPH_OPEN,kernel,iterations=5)
    gray = cv2.cvtColor(gray_img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,75,150)
    lines = cv2.HoughLinesP(edges,2,np.pi/180,10,maxLineGap=10)
    for line in lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(edges,(x1, y1),(x2, y2),(20,240,120),3)

    cv2.imshow('edge',edges)
    cv2.imshow("open",opening)

    key = cv2.waitKey(0)


cv2.destroyAllWindows()

